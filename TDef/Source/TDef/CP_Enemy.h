// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CP_Enemy.generated.h"

UENUM(BlueprintType)
enum class EType : uint8
{

	Type_Ground	UMETA(DisplayName = "Ground"),
	Type_Flying	UMETA(DisplayName = "Flying"),

};

UENUM(BlueprintType)
enum class ERarity : uint8
{

	Rarity_Ground	UMETA(DisplayName = "Normal"),
	Rarity_Flying	UMETA(DisplayName = "Boss"),

};

UCLASS()
class TDEF_API ACP_Enemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACP_Enemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Type")
	EType Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Type")
	ERarity Rarity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int32 Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float MovementSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int32 Damage;

	void MoveToWaypoints();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category	= "Stats", meta = (AllowPrivateAccess = "True"))
	int32 CurrentWaypoint;

	TArray<AActor*> Waypoints;
};
