// Fill out your copyright notice in the Description page of Project Settings.

#include "CP_Tower.h"

// Sets default values
ACP_Tower::ACP_Tower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACP_Tower::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACP_Tower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

