// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "CP_Waypoint.generated.h"

/**
 * 
 */
UCLASS()
class TDEF_API ACP_Waypoint : public AStaticMeshActor
{
	GENERATED_BODY()


public:
	int GetWaypointOrder();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = "True") )
	int32 WaypointOrder;
};
