// Fill out your copyright notice in the Description page of Project Settings.

#include "CP_Enemy.h"

// Sets default values
ACP_Enemy::ACP_Enemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


}

// Called when the game starts or when spawned
void ACP_Enemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACP_Enemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACP_Enemy::MoveToWaypoints()
{
}

// Called to bind functionality to input
void ACP_Enemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

