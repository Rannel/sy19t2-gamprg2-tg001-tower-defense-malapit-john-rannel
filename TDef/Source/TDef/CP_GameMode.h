// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CP_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class TDEF_API ACP_GameMode : public AGameModeBase
{
	GENERATED_BODY()
	
};
